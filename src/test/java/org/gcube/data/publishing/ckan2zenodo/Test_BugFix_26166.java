package org.gcube.data.publishing.ckan2zenodo;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;

import javax.ws.rs.client.Entity;

import org.gcube.data.publishing.ckan2zenodo.clients.Zenodo;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class Test_BugFix_26166.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 20, 2023
 */
@Slf4j
public class Test_BugFix_26166 {
	
	
	@Test
	public void testDate(){
		
		ZenodoDeposition zenodo = new ZenodoDeposition();
		zenodo.setCreated(Date.from(Instant.now()));
		zenodo.setModified(Date.from(Instant.now()));
		DepositionMetadata metadata = new DepositionMetadata();
		metadata.setPublication_date(Date.from(Instant.now()));
		metadata.setEmbargo_date(Date.from(Instant.now()));
		zenodo.setMetadata(metadata);

		ObjectMapper mapper = Zenodo.mapper;

		try {
			String serialized = "{\"metadata\":" + Fixer.fixIncoming(mapper.writeValueAsString(zenodo)) + "}";
			log.debug("updateMetadata - serialized: {}", serialized);
			try {
				Entity<String> jsonEnt = Entity.json(serialized);
				log.debug("updateMetadata - jsonEntity serialized: {}", jsonEnt.getEntity());
			} catch (Throwable t) {
				log.debug("Error while tryin to update " + serialized);
				throw t;
			}
		} catch (JsonProcessingException e) {
			log.debug("Error while parsing " + metadata, e);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testReadDepositionFromZenodo(){
		
		String depoisitionString = "    {\n"
				+ "        \"created\": \"2023-12-14T11:33:09.650134+00:00\",\n"
				+ "        \"modified\": \"2023-12-14T11:33:09.700320+00:00\",\n"
				+ "        \"id\": 10377042,\n"
				+ "        \"conceptrecid\": \"10377041\",\n"
				+ "        \"metadata\": {\n"
				+ "            \"title\": \"Blue-Cloud 2026 - D4.2 VLabs Implementation Guidelines\",\n"
				+ "            \"publication_date\": \"2023-12-14\",\n"
				+ "            \"description\": \"This deliverable provides guidelines on how to implement the Virtual \\nLaboratories (VLabs) in the Blue-Cloud Virtual Research Environment (VRE). \\nFirst a summary of the activities of WP4 is presented, followed by an \\noverview of the VRE services, that offer a comprehensive array of features, \\nfostering collaboration, facilitating data analytics, enabling result \\ndissemination, and ensuring seamless integration with external systems. In \\nfact, the VRE caters to the entirety of the research lifecycle.\",\n"
				+ "            \"access_right\": \"open\",\n"
				+ "            \"creators\": [\n"
				+ "                {\n"
				+ "                    \"name\": \"Assante, Massimiliano\",\n"
				+ "                    \"affiliation\": \"National Research Council of Italy - Institute of Information Science and Technologies\",\n"
				+ "                    \"orcid\": \"0000-0002-3761-1492\"\n"
				+ "                },\n"
				+ "                {\n"
				+ "                    \"name\": \"Martin-Cabrera\",\n"
				+ "                    \"affiliation\": \"Flanders Marine Institute (VLIZ)\",\n"
				+ "                    \"orcid\": \"0000-0003-3293-5612\"\n"
				+ "                }\n"
				+ "            ],\n"
				+ "            \"contributors\": [\n"
				+ "                {\n"
				+ "                    \"name\": \"Vitorino, Joao\",\n"
				+ "                    \"affiliation\": null,\n"
				+ "                    \"type\": \"Other\"\n"
				+ "                },\n"
				+ "                {\n"
				+ "                    \"name\": \"Troupin, Charles, ctroupin@uliege.be, orcid.org/\",\n"
				+ "                    \"affiliation\": \"Université de Liège\",\n"
				+ "                    \"type\": \"Other\",\n"
				+ "                    \"orcid\": \"0000-0002-0265-1021\"\n"
				+ "                },\n"
				+ "                {\n"
				+ "                    \"name\": \"Pint, Steven\",\n"
				+ "                    \"affiliation\": \"Flanders Marine Institute (VLIZ)\",\n"
				+ "                    \"type\": \"Other\",\n"
				+ "                    \"orcid\": \"0000-0001-5530-6785\"\n"
				+ "                },\n"
				+ "                {\n"
				+ "                    \"name\": \"Palermo, Francesco\",\n"
				+ "                    \"affiliation\": \"Centro Euro-Mediterraneo sui Cambiamenti Climatici (CMCC)\",\n"
				+ "                    \"type\": \"Other\",\n"
				+ "                    \"orcid\": \"0000-0002-0463-4655\"\n"
				+ "                },\n"
				+ "                {\n"
				+ "                    \"name\": \"Grasset, Bastien\",\n"
				+ "                    \"affiliation\": \"French National Research Institute for Sustainable Development (IRD)\",\n"
				+ "                    \"type\": \"Other\"\n"
				+ "                },\n"
				+ "                {\n"
				+ "                    \"name\": \"Barth, Alexander\",\n"
				+ "                    \"affiliation\": \"Université de Liège\",\n"
				+ "                    \"type\": \"Other\",\n"
				+ "                    \"orcid\": \"0000-0003-2952-5997\"\n"
				+ "                },\n"
				+ "                {\n"
				+ "                    \"name\": \"Barde, Julien\",\n"
				+ "                    \"affiliation\": \"French National Research Institute for Sustainable Development (IRD)\",\n"
				+ "                    \"type\": \"Other\",\n"
				+ "                    \"orcid\": \"0000-0002-3519-6141\"\n"
				+ "                }\n"
				+ "            ],\n"
				+ "            \"keywords\": [\n"
				+ "                \"VLabs\",\n"
				+ "                \"VRE\"\n"
				+ "            ],\n"
				+ "            \"related_identifiers\": [\n"
				+ "                {\n"
				+ "                    \"identifier\": \"https://data.d4science.org/ctlg/Blue-Cloud2026Project/d4_2_vlabs_implementation_guidelines\",\n"
				+ "                    \"relation\": \"isCompiledBy\",\n"
				+ "                    \"scheme\": \"url\"\n"
				+ "                }\n"
				+ "            ],\n"
				+ "            \"version\": \"1\",\n"
				+ "            \"grants\": [\n"
				+ "                {\n"
				+ "                    \"id\": \"10.13039/501100000780::101094227\"\n"
				+ "                }\n"
				+ "            ],\n"
				+ "            \"license\": \"cc-by-4.0\",\n"
				+ "            \"imprint_publisher\": \"Zenodo\",\n"
				+ "            \"communities\": [\n"
				+ "                {\n"
				+ "                    \"identifier\": \"bluecloud\"\n"
				+ "                }\n"
				+ "            ],\n"
				+ "            \"upload_type\": \"publication\",\n"
				+ "            \"publication_type\": \"deliverable\",\n"
				+ "            \"prereserve_doi\": {\n"
				+ "                \"doi\": \"10.5281/zenodo.10377042\",\n"
				+ "                \"recid\": 10377042\n"
				+ "            }\n"
				+ "        },\n"
				+ "        \"title\": \"Blue-Cloud 2026 - D4.2 VLabs Implementation Guidelines\",\n"
				+ "        \"links\": {\n"
				+ "            \"self\": \"https://zenodo.org/api/records/10377042\",\n"
				+ "            \"html\": \"https://zenodo.org/records/10377042\",\n"
				+ "            \"badge\": \"https://zenodo.org/badge/doi/.svg\",\n"
				+ "            \"files\": \"https://zenodo.org/api/records/10377042/files\",\n"
				+ "            \"latest_draft\": \"https://zenodo.org/api/deposit/depositions/10377042\",\n"
				+ "            \"latest_draft_html\": \"https://zenodo.org/deposit/10377042\",\n"
				+ "            \"publish\": \"https://zenodo.org/api/deposit/depositions/10377042/actions/publish\",\n"
				+ "            \"edit\": \"https://zenodo.org/api/deposit/depositions/10377042/actions/edit\",\n"
				+ "            \"discard\": \"https://zenodo.org/api/deposit/depositions/10377042/actions/discard\",\n"
				+ "            \"newversion\": \"https://zenodo.org/api/deposit/depositions/10377042/actions/newversion\",\n"
				+ "            \"registerconceptdoi\": \"https://zenodo.org/api/deposit/depositions/10377042/actions/registerconceptdoi\"\n"
				+ "        },\n"
				+ "        \"record_id\": 10377042,\n"
				+ "        \"owner\": 82231,\n"
				+ "        \"files\": [],\n"
				+ "        \"state\": \"unsubmitted\",\n"
				+ "        \"submitted\": false\n"
				+ "    }";
		
		try {
			ObjectMapper mapper = Zenodo.mapper;
			ZenodoDeposition dep = mapper.readValue(Fixer.fixIncoming(depoisitionString), ZenodoDeposition.class);
			System.out.println("The deposition: "+dep);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
