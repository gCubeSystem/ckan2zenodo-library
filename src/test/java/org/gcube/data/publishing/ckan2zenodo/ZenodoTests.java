package org.gcube.data.publishing.ckan2zenodo;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.data.publishing.ckan2zenodo.clients.Zenodo;
import org.gcube.data.publishing.ckan2zenodo.commons.IS;
import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.CkanRelatedIdentifier;
import org.gcube.data.publishing.ckan2zenodo.model.CkanResource;
import org.gcube.data.publishing.ckan2zenodo.model.DownloadedFile;
import org.gcube.data.publishing.ckan2zenodo.model.ZenodoCredentials;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ZenodoException;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.Creator;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata.AccessRights;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata.UploadType;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.FileDeposition;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jayway.jsonpath.JsonPath;


public class ZenodoTests {
	
	private static ZenodoCredentials credentials;	
	private static Map<String,String> mappings;
	
	@BeforeClass
	public static final void init() throws IOException {
		credentials=TestCommons.loadCredentials();
		mappings=TestCommons.getDefinedMappings();
	}
	
	
	@Test
	public void readFromZenodo() throws IOException, ZenodoException {
		
//		private static final String BASE_URL="https://zenodo.org/api/";
		
		Zenodo z=new Zenodo(credentials);
		
		System.out.println(z.readDeposition(472904));
		
		
	}

	
	
	@Test
	public void createDeposition() throws ZenodoException, JsonProcessingException {
		Zenodo z=new Zenodo(credentials);
		ZenodoDeposition dep=z.createNew();
		dep.setMetadata(new DepositionMetadata(UploadType.dataset,
				new Date(System.currentTimeMillis()),
				"Test",
				Arrays.asList(new Creator("simpleMan")),
				"Simple description",
				AccessRights.open));
		System.out.println(z.updateMetadata(dep));
		
	}
	
	@Test
	public void createFromSimpleItem() throws ConfigurationException, Exception {
		Zenodo z=new Zenodo(credentials);
		
		GenericResource res=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream("/ResearchObject.xml"));
		ZenodoDeposition dep=z.createNew();
		Translator tran=new Translator(IS.readMappings(res));
		dep=TestCommons.readAndTransform("/ResearchObject.json", tran,dep);
				dep=z.updateMetadata(dep);
				
		System.out.println("Resulting Deposition with metadata : ");
		System.out.println();
		String json=TestCommons.convertStreamToString(TransformationTests.class.getResourceAsStream("/ResearchObject.json"));		
		CkanItemDescriptor desc=new CkanItemDescriptor(json);
		
		for(CkanResource cRes:tran.filterResources(desc)) {
			DownloadedFile f=new DownloadedFile(cRes);
			FileDeposition file=z.uploadFile(dep, cRes.getName(), f.getFile());
			System.out.println("Published "+file);
		}
				
				
		dep=z.publish(dep);
		
		System.out.println();		
	}
	
	@Test
	public void updateExisting() throws ConfigurationException, Exception {
		// publish new
		Zenodo z=new Zenodo(credentials);
		
		GenericResource res=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream("/ResearchObject.xml"));
		ZenodoDeposition dep=z.createNew();
		Translator tran=new Translator(IS.readMappings(res));
		dep=TestCommons.readAndTransform("/ResearchObject.json", tran,dep);
				dep=z.updateMetadata(dep);
				
		System.out.println("Resulting Deposition with metadata : ");
		System.out.println();
		String json=TestCommons.convertStreamToString(TransformationTests.class.getResourceAsStream("/ResearchObject.json"));		
		CkanItemDescriptor desc=new CkanItemDescriptor(json);
		
		for(CkanResource cRes:tran.filterResources(desc)) {
			DownloadedFile f=new DownloadedFile(cRes);
			
			FileDeposition file=z.uploadFile(dep, cRes.getName(), f.getFile());
			System.out.println("Published "+file);
		}		
		dep=z.publish(dep);
		Assert.assertTrue("Invalid deposition state after publishing ",dep.getState().equals("done"));
		Assert.assertTrue("Invalid submitted state after publishing", dep.getSubmitted());
		
		desc.setZenodoDoi(CkanRelatedIdentifier.getZenodo(dep.getDOIUrl()));
		
		// Change title
		String fakeTitle="Dummy Title";		
		
		String modifiedJson=JsonPath.parse(desc.getContent()).put("$", "title", fakeTitle).jsonString();
		desc=new CkanItemDescriptor(modifiedJson);
		// Publish logic : 
		
		Assert.assertNotNull(desc);
		Assert.assertNotNull(desc.getZenodoDoi());
		Assert.assertEquals(fakeTitle, desc.getTitle());		
		
		dep=z.readDeposition(desc.getZenodoDoi().getZenodoId());
		dep=tran.transform(desc, dep);
		z.unlockPublished(dep.getId());
		System.out.println(z.updateMetadata(dep));
		dep=z.publish(dep);
		Assert.assertEquals(fakeTitle, dep.getTitle());
		System.out.println(dep);
	}
	
	
	@Test
	public void testMappings() throws Exception {
		Zenodo z=new Zenodo(credentials);
		
		for(Entry<String,String> entry:mappings.entrySet()) {
			GenericResource resource=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream(entry.getValue()));
			Translator translator=new Translator(IS.readMappings(resource));
			ZenodoDeposition dep=TestCommons.readAndTransform(entry.getKey(), translator,z.createNew());
			dep=z.updateMetadata(dep);
			System.out.println("Created "+dep);
			//No payload to publish
//			System.out.println("Published "+z.publish(dep));
		}
	}
}
