package org.gcube.data.publishing.ckan2zenodo;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.gcube.data.publishing.ckan2zenodo.clients.GCat;
import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.CkanResource;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.GcatException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.InvalidItemException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.TransformationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ZenodoException;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;
import org.gcube.gcat.client.Item;

public class OneHitTest {


	public static void main(String[] args) throws GcatException, InvalidItemException, ZenodoException, ConfigurationException, TransformationException, InterruptedException, ExecutionException, MalformedURLException {
		//		String scope="/gcube/devsec";
		//		String scope="/pred4s/preprod/preVRE";
		//		String scope="/d4science.research-infrastructures.eu/D4OS/Blue-CloudLab";
		//		TokenSetter.set(scope);
		//
		//
		//		//		String toPublishItemName="a_sample_deliverable_4_zenodo";
		//		String toPublishItemName="blue_cloud_demonstrator_users_handbook_v1";

		// Get the item representation

		// Import & TEST 

		String sourceContext="/d4science.research-infrastructures.eu/D4OS/Blue-CloudProject";
		String targetContext="/pred4s/preprod/preVRE";

		Boolean publish=true;
		String[] items= {
				"blue_cloud_demonstrator_users_handbook_v1"
		};

		// IMPORT
				importItemsByProfile(sourceContext, targetContext, items).forEach((CkanItemDescriptor d)->{
					try {
						testFullCycle(targetContext, d.getName(), publish);
					} catch (GcatException | InvalidItemException | ZenodoException | ConfigurationException
							| TransformationException | InterruptedException | ExecutionException | MalformedURLException e) {
						throw new RuntimeException(e);
					}
				});;

//		testFullCycle(sourceContext, items[0], false);

	}



	public static void testFullCycle(String context,String toPublishItemName, boolean publish) throws GcatException, InvalidItemException, ZenodoException, ConfigurationException, TransformationException, InterruptedException, ExecutionException, MalformedURLException {
		TokenSetter.set(context);
		Ckan2Zenodo client=new Ckan2ZenodoImpl();
		CkanItemDescriptor item=client.read(toPublishItemName);

		System.out.println("Item PROFILE is : "+item.getProfile());
		//Get a preview of the deposition to be published
		ZenodoDeposition preview=client.translate(item);
		
		System.out.println("DEPOSITION SOULD BE "+preview);
		//Filter resources according to VRE policies
		List<CkanResource> toFilter=client.filterResources(item);

		//Eventually update values
		preview.getMetadata().setAccess_conditions("Ask me");

		
		
		//Actually publish to zenodo : 
		// Step 1 : metadata
		if(publish) {
			System.out.println("!!!! PUBLISHING.. ");
			//Finalize
			try{
				Thread.sleep(5000);
			}catch(InterruptedException e) {}
			
			
			
			System.out.println("Create/update metadata..");
			preview=client.updatedMetadata(preview);

			System.out.println("Publish files..");
			//Step 2 : publish Resources 
			Future<ZenodoDeposition> future_Dep=client.uploadFiles(Collections.singleton(toFilter.get(0)), preview);
			preview=future_Dep.get();

			System.out.println("DONE : "+client.publish(preview, item));
		}
	}


	public static ArrayList<CkanItemDescriptor> importItemsByProfile(String sourceContext,String targetContext,String... items) throws MalformedURLException, GcatException {
		System.out.println("Importing from "+sourceContext);
		TokenSetter.set(sourceContext);
		ArrayList<CkanItemDescriptor> toReturn=new ArrayList<CkanItemDescriptor>();
		for(String s:items)
			toReturn.add(GCat.getByID(s));

		System.out.println("Pushing in "+targetContext);
		TokenSetter.set(targetContext);


		for(CkanItemDescriptor d:toReturn) {
			try{
				d.removeOrganization();
				d.removeId();
				//				List<CkanResource> res=
				d.getResources();
				d.cleanResources();


				new Item().create(d.getContent());
			}catch(Exception e) {
				System.err.println("Cannot publish "+d.getContent());
				e.printStackTrace(System.err);
				GCat.updateItem(d);
			}
		}
		return toReturn;
	}
}
