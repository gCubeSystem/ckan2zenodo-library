package org.gcube.data.publishing.ckan2zenodo;

import org.gcube.data.publishing.ckan2zenodo.clients.Zenodo;
import org.gcube.data.publishing.ckan2zenodo.model.ZenodoCredentials;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;

public class InspectCredentials {

	public static void main(String[] args) throws ConfigurationException {
		TokenSetter.set("/d4science.research-infrastructures.eu/D4Research/AGINFRAplusDev");

		ZenodoCredentials credentials=Zenodo.get().getCredentials();
		
		System.out.println(credentials.getBaseUrl());
		System.out.println(credentials.getKey());
		
	}

}
