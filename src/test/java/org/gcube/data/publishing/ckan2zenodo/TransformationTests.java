package org.gcube.data.publishing.ckan2zenodo;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.data.publishing.ckan2zenodo.commons.IS;
import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.CkanResource;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.parsing.Filter;
import org.gcube.data.publishing.ckan2zenodo.model.parsing.Mapping;
import org.gcube.data.publishing.ckan2zenodo.model.parsing.Mappings;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TransformationTests {

	static ObjectMapper mapper=null;
	
	static Map<String,String> mappings;
	
	@BeforeClass
	public static void init () {
		mapper=TestCommons.getMapper();
		
		mappings=TestCommons.getDefinedMappings();
	}
	
	
	@Test
	public void testUnmarshalling() throws ConfigurationException {
		List<String> toCheck=new ArrayList<String>();		
		toCheck.add("/simple.xml");
		toCheck.addAll(mappings.values());
		for(String resFile:toCheck) {
			System.out.println("Checking "+resFile);
			GenericResource resource=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream(resFile));
			Mappings m=IS.readMappings(resource);
			assertTrue(m.getResourceFilters()!=null);
			assertTrue(m.getResourceFilters().getFilters()!=null);
			m.getResourceFilters().getFilters().forEach((Filter f)->{
				assertTrue(f.getConditions()!=null&&!f.getConditions().isEmpty());});
			assertTrue(m.getMappings()!=null);
			m.getMappings().forEach((Mapping map)->{
//				assertTrue(map.getSource()!=);				
			});
			new Translator(m);
		}
	}
	@Test 
	public void testMarhalling() {
		
		
//		Mappings m=new Mappings();
//		Mapping m1=new Mapping();
//		m1.
//		
//		m.getMappings().add(new Mapping());
//		
		
	}
	
	
	@Test
	public void transform() throws Exception {
		Translator basic=new Translator();
		TestCommons.readAndTransform("/simpleItem.json",basic);
		TestCommons.readAndTransform("/FSKXModel.json",basic);
		TestCommons.readAndTransform("/ResearchObject.json",basic);		
		
	}
	
	
	@Test
	public void transformWithMappings() throws Exception {
		
		GenericResource res=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream("/ResearchObject.xml"));
		Translator t=new Translator(IS.readMappings(res));
//		TestCommons.readAndTransform("/ResearchObject.json", t);
//		TestCommons.readAndTransform("/crop_parameters.json",t);
		
		
		
		for(Entry<String,String> entry:mappings.entrySet()) {
			GenericResource resource=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream(entry.getValue()));
			Translator translator=new Translator(IS.readMappings(resource));
			TestCommons.readAndTransform(entry.getKey(), translator);
		}
		
		
	}	
	
	@Test
	public void filterResources() throws Exception {
		GenericResource res=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream("/ResearchObject.xml"));
		Translator t=new Translator(IS.readMappings(res));
		
		String json=TestCommons.convertStreamToString(TransformationTests.class.getResourceAsStream("/ResearchObject.json"));		
		CkanItemDescriptor desc=new CkanItemDescriptor(json);
		
		for(CkanResource filtered:t.filterResources(desc)) 
			System.out.println("NAME : "+filtered.getName()+ " FORMAT : "+filtered.getFormat());
			
		
	}
	
	
}
