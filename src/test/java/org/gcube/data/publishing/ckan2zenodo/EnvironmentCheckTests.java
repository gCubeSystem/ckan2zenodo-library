package org.gcube.data.publishing.ckan2zenodo;

import org.gcube.data.publishing.ckan2zenodo.model.faults.*;
import org.gcube.data.publishing.ckan2zenodo.model.report.EnvironmentReport;
import org.junit.Assume;
import org.junit.Test;

public class EnvironmentCheckTests {

    String context="/pred4s/preprod/preVRE";

    @Test
    public void failCheckEnvironemnt(){
        TokenSetter.set("/gcube");
        Ckan2Zenodo client=new Ckan2ZenodoImpl();
        EnvironmentReport report=client.checkEnvironment();
        Assume.assumeFalse(report.isok());
        System.out.println(report);
    }

    @Test
    public void okCheckEnvironemnt(){
        TokenSetter.set(context);
        Ckan2Zenodo client=new Ckan2ZenodoImpl();
        EnvironmentReport report=client.checkEnvironment();
        Assume.assumeTrue(report.isok());
        System.out.println(report);
    }

    @Test
    public void checkMappings() throws GcatException, TransformationException, ConfigurationException, InvalidItemException, ZenodoException {
        TokenSetter.set(context);
        Ckan2Zenodo client=new Ckan2ZenodoImpl();

        client.translate(client.read("knime_workflow_with_joined_consumer_phase_and_dose_response_model"));
//        client.translate(client.read("lwr_in_prevre"));
    client.translate(client.read("xyextractor_in_prevre"));

    }
}
