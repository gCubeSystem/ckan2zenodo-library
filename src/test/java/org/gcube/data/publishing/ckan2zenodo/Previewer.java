package org.gcube.data.publishing.ckan2zenodo;

import java.io.FileOutputStream;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.data.publishing.ckan2zenodo.commons.IS;
import org.gcube.data.publishing.ckan2zenodo.model.parsing.Mappings;
import org.junit.Test;

public class Previewer {

	
	@Test
	public void preview() throws Exception {
		String sourceItem="/blue_cloud_deliverable.json";
		String genResource="/blue_cloud_deliverable.xml";
		
		GenericResource resource=Resources.unmarshal(GenericResource.class, TransformationTests.class.getResourceAsStream(genResource));
		Mappings m=IS.readMappings(resource);
		
		try(FileOutputStream fos = new FileOutputStream("out.json")){		
			TestCommons.getMapper().writeValue(fos, TestCommons.readAndTransform(sourceItem, new Translator(m)));
		};
		
		
	}
	
	
}
