package org.gcube.data.publishing.ckan2zenodo;

import java.net.MalformedURLException;

import javax.ws.rs.WebApplicationException;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.data.publishing.ckan2zenodo.clients.GCat;
import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.faults.GcatException;
import org.gcube.gcat.client.Item;
import org.gcube.gcat.client.Profile;
import org.junit.Test;

public class GCatTests {

	
	@Test
	public void getProfile() throws MalformedURLException, GcatException {
		TokenSetter.set("/pred4s/preprod/preVRE");
		System.out.println(SecurityTokenProvider.instance.get());
		CkanItemDescriptor item=GCat.getByID("sampleontable_in_prevre");
		
		
		System.out.println(item.getProfile());
	}
	@Test
	public void listProfiles() throws WebApplicationException, MalformedURLException {
		TokenSetter.set("/gcube/devsec");
		System.out.println(SecurityTokenProvider.instance.get());
		System.out.println("PROFILES : ");
		System.out.println(new Profile().list());
	}
	
	@Test
	public void publishUpdate() throws MalformedURLException {
		TokenSetter.set("/gcube/devsec/devVRE");
		System.out.println(SecurityTokenProvider.instance.get());
		String json=TestCommons.convertStreamToString(GCatTests.class.getResourceAsStream("/ResearchObject.json"));
		new Item().create(json);
//		GCat.updateItem(toUpdate);
	}
	
	
}
