package org.gcube.data.publishing.ckan2zenodo.model;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class NetTests {

    @Test
    public void testfileNames() throws Exception {

        CkanResource res=new CkanResource();
        res.setName("Deliverable");
        res.setDescription("My description");
        res.setId("resource_id");

        // PDF URL
        res.setUrl("https://data-pre.d4science.net/RgA7");
        check(new DownloadedFile(res),"Deliverable.pdf",true);

        
        // Do not use HEAD if extension in resource name
        res.setName("Deliverable.rtf");
        check(new DownloadedFile(res),res.getName(),true);

        //Check invalid urls i.e. folder url == UNABLE TO GET FILENAME FROM HEAD
        res.setUrl("http://data-pre.d4science.org/workspace-explorer-app?folderId=UjV1MTJ4K2lvQU5MRE1MT2NCOEVGWDkvMG5SL2dwY3A0QmpWZmdRVEFxR3Njd2cwcUxUQ3BBZzZxa1FhN3JQTQ");
            // Still should use resource name
        check(new DownloadedFile(res),res.getName(),true);

        //Check invalid urls i.e. folder url == UNABLE TO GET FILENAME FROM HEAD
        res.setName("Deliverable");
           //Shouldn't have extension
        check(new DownloadedFile(res),res.getName(),false);
    }


    private static final void check(DownloadedFile f, String expectedFilename, boolean expectExtension) throws Exception {
        System.out.println(f);
        System.out.println("Resulting filename is : "+f.getToUseFileName());
        assertTrue(f.getToUseFileName()!=null);
        if(expectExtension)
            assertTrue(DownloadedFile.getExtension(f.getToUseFileName())!=null);
        else assertTrue(DownloadedFile.getExtension(f.getToUseFileName())==null);
        assertTrue(f.getToUseFileName().equals(expectedFilename));
    }

}
