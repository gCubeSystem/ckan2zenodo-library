package org.gcube.data.publishing.ckan2zenodo;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.CkanResource;
import org.gcube.data.publishing.ckan2zenodo.model.ZenodoCredentials;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class TestCommons {

	static ZenodoCredentials loadCredentials() throws IOException {
		String BASE_URL="https://sandbox.zenodo.org/api/";
		
		String key=Files.readAllLines(Paths.get("/Users/FabioISTI/eclipse-workspace/zenodo-sandbox.key")).get(0);
		
		System.out.println("Using key : "+key);
		return new ZenodoCredentials(key,BASE_URL);
	}
	
	
	static Map<String,String> getDefinedMappings(){
		HashMap<String,String> toReturn=new HashMap<String,String>();
		toReturn.put("/blue_cloud_dataset.json", "/blue_cloud_dataset.xml");
		toReturn.put("/blue_cloud_deliverable.json", "/blue_cloud_deliverable.xml");
		return toReturn;
	}
	
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	
	static ObjectMapper getMapper() {
		ObjectMapper mapper=new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		mapper.setSerializationInclusion(Include.NON_NULL);
		return mapper;
	}
	
	static final ZenodoDeposition readAndTransform(String jsonFile, Translator transformer,ZenodoDeposition...depositions) throws Exception {
		return readAndTransform(jsonFile, transformer, System.out, depositions);
	}
	
	static final ZenodoDeposition readAndTransform(String jsonFile, Translator transformer,PrintStream os, ZenodoDeposition...depositions) throws Exception {
		try{
			String json=TestCommons.convertStreamToString(TransformationTests.class.getResourceAsStream(jsonFile));		
		CkanItemDescriptor desc=new CkanItemDescriptor(json);
		os.println("Going to transform : "+desc.getContent());
		os.println("Result : ");
		ZenodoDeposition dep=transformer.transform(desc, (depositions!=null&&depositions.length>0)?depositions[0]:null);		
		os.println(dep);
		os.println("As JSON : ");
		os.println(Fixer.fixSending(getMapper().writeValueAsString(dep)));
		os.println("Filtered resources : ");
		for(CkanResource res : transformer.filterResources(desc)) {
			os.println("AS JSON : ");
			os.println(Fixer.fixSending(getMapper().writeValueAsString(res)));
		}
		return dep;
		}catch(Throwable t) {
			throw new Exception("Errors with json file "+jsonFile,t);			
		}
	}

}
