package org.gcube.data.publishing.ckan2zenodo;

import java.io.IOException;
import java.util.List;

import org.gcube.data.publishing.ckan2zenodo.commons.Parsing;
import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.CkanRelatedIdentifier;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.InvalidItemException;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class ParsingTests {

	static ObjectMapper mapper=null;
	@BeforeClass
	public static void init () {
		mapper=TestCommons.getMapper();
	}


	@Test
	public void DateTest() throws Exception {
		for(String d:new String[] {"2019-11-29T14:54:42.542142","2016-06-15T16:10:03.319363+00:00","2019-11-30","2019-11-29T17:01:31.000160+0000"}) {
			System.out.println(d);
			System.out.println(Fixer.fixIncomingDateString(d));
			System.out.println("--");
		}
	}

	@Test
	public void fullCircleFromIncoming() throws JsonParseException, JsonMappingException, IOException {
		String s="{\n" + 
				"    \"conceptrecid\": \"426311\",\n" + 
				"    \"created\": \"2019-11-29T14:54:42.542142\",\n" + 
				"    \"doi\": \"\",\n" + 
				"    \"doi_url\": \"https://doi.org/\",\n" + 
				"    \"id\": 426312,\n" + 
				"    \"links\":\n" + 
				"    {\n" + 
				"        \"discard\": \"https://sandbox.zenodo.org/api/deposit/depositions/426312/actions/discard\",\n" + 
				"        \"edit\": \"https://sandbox.zenodo.org/api/deposit/depositions/426312/actions/edit\",\n" + 
				"        \"files\": \"https://sandbox.zenodo.org/api/deposit/depositions/426312/files\",\n" + 
				"        \"html\": \"https://sandbox.zenodo.org/deposit/426312\",\n" + 
				"        \"latest_draft\": \"https://sandbox.zenodo.org/api/deposit/depositions/426312\",\n" + 
				"        \"latest_draft_html\": \"https://sandbox.zenodo.org/deposit/426312\",\n" + 
				"        \"publish\": \"https://sandbox.zenodo.org/api/deposit/depositions/426312/actions/publish\",\n" + 
				"        \"self\": \"https://sandbox.zenodo.org/api/deposit/depositions/426312\"\n" + 
				"    },\n" + 
				"    \"metadata\":\n" + 
				"    {\n" + 
				"        \"access_right\": \"open\",\n" + 
				"        \"creators\": [\n" + 
				"        {\n" + 
				"            \"name\": \"simpleMan\"\n" + 
				"        }],\n" + 
				"        \"description\": \"Simple description\",\n" + 
				"        \"doi\": \"\",\n" + 
				"        \"license\": \"CC0-1.0\",\n" + 
				"        \"prereserve_doi\":\n" + 
				"        {\n" + 
				"            \"doi\": \"10.5072/zenodo.426312\",\n" + 
				"            \"recid\": 426312\n" + 
				"        },\n" + 
				"        \"publication_date\": \"2018-12-30\",\n" + 
				"        \"title\": \"Test\",\n" + 
				"        \"upload_type\": \"dataset\"\n" + 
				"    },\n" + 
				"    \"modified\": \"2019-11-29T14:54:43.203022\",\n" + 
				"    \"owner\": 31041,\n" + 
				"    \"record_id\": 426312,\n" + 
				"    \"state\": \"unsubmitted\",\n" + 
				"    \"submitted\": false,\n" + 
				"    \"title\": \"Test\"\n" + 
				"}";
		System.out.println("Sample String : ");
		System.out.println(s);
		System.out.println("READ (toString): ");
		ZenodoDeposition dep=mapper.readValue(Fixer.fixIncoming(s), ZenodoDeposition.class);
		System.out.println(dep);
		System.out.println("READ (JSON): ");
		String json=mapper.writeValueAsString(dep);
		System.out.println(json);
		System.out.println("FIXED JSON ");
		String fixed=Fixer.fixSending(json);
		System.out.println(fixed);

	}

	@Test
	public void miscTests() throws IOException, ConfigurationException {
//		ObjectMapper mapper=Parsing.getMapper();
//		mapper.setSerializationInclusion(Include.ALWAYS);
		ZenodoDeposition dep=new ZenodoDeposition();
		dep.setMetadata(new DepositionMetadata());


		DocumentContext sourceCtx=JsonPath.using(Parsing.JSON_PATH_ALWAYS_LIST_CONFIG).parse(mapper.writeValueAsString(dep));
		Parsing.addElement(sourceCtx,"$.metadata.creators[0]");
//		addElement(sourceCtx,"$.metadata.creators[0].name",true);
		sourceCtx.put("$.metadata.creators[0]","name", "myName");
		System.out.println("JSON : "+sourceCtx.jsonString());
		Assert.assertTrue(sourceCtx.jsonString().contains("creators"));		
		Assert.assertTrue(sourceCtx.jsonString().contains("myName"));
		ZenodoDeposition parsed=mapper.readValue(sourceCtx.jsonString(), ZenodoDeposition.class);
		Assert.assertEquals(parsed.getMetadata().getCreators().get(0).getName(), "myName");

	}

	
	
	@Test
	public void read() {
		DocumentContext sourceCtx=JsonPath.using(Parsing.JSON_PATH_ALWAYS_LIST_CONFIG).parse("{\"metadata\":{\"creators\":[{\"name\":\"\"}]}}");
		List<Object> found=sourceCtx.read("$['metadata']['creators'][0]");
		for(Object s: found) {
			
			System.out.println(s);
		}
	}
	
	
	@Test
	public void addDOI() throws InvalidItemException {
		String json=TestCommons.convertStreamToString(TransformationTests.class.getResourceAsStream("/ResearchObject.json"));		
		CkanItemDescriptor desc=new CkanItemDescriptor(json);
		Assert.assertNull(desc.getZenodoDoi());
		desc.setZenodoDoi(new CkanRelatedIdentifier("URL", "isReferencedBy", "http://10.5281/zenodo.1000001"));
		CkanRelatedIdentifier id=desc.getZenodoDoi();
		Assert.assertNotNull(id);
		System.out.println(desc.getContent());
	}
	
	@Test
	public void guiTest() throws JsonParseException, JsonMappingException, IOException {
		String toParse="{\n" + 
				"    \"metadata\":\n" + 
				"    {\n" + 
				"        \"title\": \"Crop Parameters\",\n" + 
				"        \"description\": \"Parameter definitions for various crops. Can be used as input for a crop \\r\\nsimulation model, such as WOFOST.\",\n" + 
				"        \"access_right\": \"open\",\n" + 
				"        \"license\": \"AFL-3.0\",\n" + 
				"        \"keywords\": [\"crop parameters\", \"crop simulation\", \"input files\", \"wofost\"],\n" + 
				"        \"related_identifiers\": [\n" + 
				"        {\n" + 
				"            \"identifier\": \"http://data.d4science.org/ctlg/AgroClimaticModelling/crop_parameters\",\n" + 
				"            \"relation\": \"compiles\"\n" + 
				"        }],\n" + 
				"        \"contributors\": [\n" + 
				"        {\n" + 
				"            \"type\": \"Producer\",\n" + 
				"            \"name\": \"Knapen Rob\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"type\": \"DataCurator\",\n" + 
				"            \"name\": \"Knapen Rob\"\n" + 
				"        }],\n" + 
				"        \"version\": \"1\",\n" + 
				"        \"upload_type\": \"dataset\",\n" + 
				"        \"creators\": [\n" + 
				"        {\n" + 
				"            \"name\": \"Knapen Rob\",\n" + 
				"            \"type\": \"Producer\"\n" + 
				"        }]\n" + 
				"    }\n" + 
				"}";
		ZenodoDeposition dep=Parsing.getMapper().readValue(toParse,ZenodoDeposition.class);
		System.out.println(dep);
	}
}
