package org.gcube.data.publishing.ckan2zenodo.clients;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.core.Response;

import org.gcube.data.publishing.ckan2zenodo.LocalConfiguration;
import org.gcube.data.publishing.ckan2zenodo.LocalConfiguration.Configuration;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;

public class FileUploaderManager {

	static ExecutorService uploadService=null;
	static ExecutorService requestService=null;
	
	
	static {
		uploadService=Executors.newFixedThreadPool(Integer.parseInt(LocalConfiguration.getProperty(Configuration.THREAD_POOL_SIZE))*3);
		requestService=Executors.newFixedThreadPool(Integer.parseInt(LocalConfiguration.getProperty(Configuration.THREAD_POOL_SIZE)));
	}
	
	public static Future<ZenodoDeposition> submitForDeposition(Callable<ZenodoDeposition> call ){
		return requestService.submit(call);
	}
	
	
	public static Future<Response> submitForResponse(Callable<Response> call){
		return uploadService.submit(call);
	}

}
