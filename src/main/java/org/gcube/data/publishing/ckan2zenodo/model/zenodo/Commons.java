package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

public class Commons {

	public static final String ISO_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ";

	// Added by Francesco M. #26166
	public static final String ISO_DATE_PATTERN_WITHOUT_TIME = "yyyy-MM-dd";

}
