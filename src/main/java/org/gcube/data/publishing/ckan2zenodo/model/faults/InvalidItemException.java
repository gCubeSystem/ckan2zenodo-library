package org.gcube.data.publishing.ckan2zenodo.model.faults;

public class InvalidItemException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 956698656847887456L;

	public InvalidItemException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidItemException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public InvalidItemException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public InvalidItemException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public InvalidItemException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
