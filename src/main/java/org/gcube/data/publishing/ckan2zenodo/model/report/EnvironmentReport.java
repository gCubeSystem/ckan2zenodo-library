package org.gcube.data.publishing.ckan2zenodo.model.report;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class EnvironmentReport {


    private HashMap<String,Status> reportItems=new HashMap<>();

    private String context;

    public boolean isok(){
        for(Map.Entry<String,Status> entry:reportItems.entrySet())
            if(entry.getValue().equals(Status.ERROR)) return false;
        return true;
    }
}
