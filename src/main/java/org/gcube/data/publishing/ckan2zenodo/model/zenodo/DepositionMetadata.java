package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@RequiredArgsConstructor
public class DepositionMetadata {

	public static enum UploadType{
		publication,
		poster,
		presentation,
		dataset,
		image,
		video,
		software,
		lesson,
		other		
	}
	
	public static enum PublicationType{
		annotationcollection,
		book,
		section,
		conferencepaper,
		datamanagementplan,
		article,
		patent,
		preprint,
		deliverable,
		milestone,
		proposal,
		report,
		softwaredocumentation,
		taxonomictreatment,
		techincalnote,
		thesis,
		workingpaper,
		other
	}
	
	public static enum ImageType{
		figure,
		plot,
		drawing,
		diagram,
		photo,
		other
	}
	
	public static enum AccessRights{
		open,
		embargoed,
		restricted,
		closed
	}
	
//	@NonNull	
	private UploadType upload_type;
	private PublicationType publication_type; 
	private ImageType image_type; 
//	@NonNull - Updated by Francesco M. 
	@JsonFormat(pattern = Commons.ISO_DATE_PATTERN_WITHOUT_TIME)
	private Date publication_date;
//	@NonNull
	private String title;
//	@NonNull
	private List<Creator> creators;
//	@NonNull
	private String description; // TODO HTML
//	@NonNull
	private AccessRights access_right;
	private String license; // TODO ENUM https://licenses.opendefinition.org/licenses/groups/all.json
	@JsonFormat(pattern = Commons.ISO_DATE_PATTERN_WITHOUT_TIME)
	private Date embargo_date;
	private String access_conditions; // TODO HTML
	private String doi;
	private Boolean preserve_doi;
	private List<String> keywords;
	private String notes; // TODO HTML
	private List<RelatedIdentifier> related_identifiers;
	private List<Contributor> contributors;
	private List<String> references;
	private List<Community> communities;
	private List<Grant> grants;
	private String journal_title;
	private String journal_volume;
	private String journal_issue;
	private String journal_pages;
	private String conference_title;
	private String conference_acronym;
	private String conference_dates;
	private String conference_place;
	private String conference_url;
	private String conference_session;
	private String conference_session_part;
	private String imprint_publisher;
	private String imprint_isbn;
	private String imprint_place;
	private String partof_title;
	private String partof_pages;
	private List<Creator> thesis_supervisors;
	private String thesis_university;
	private List<Subject> subjects;
	private String version;
	private String language; //https://www.loc.gov/standards/iso639-2/php/code_list.php
	private List<Location> locations;
	private List<DateInterval> dates;
	private String method; //TODO html
	
	public DepositionMetadata(UploadType upload_type, Date publication_date, String title, List<Creator> creators,
			String description, AccessRights access_right) {
		super();
		this.upload_type = upload_type;
		this.publication_date = publication_date;
		this.title = title;
		this.creators = creators;
		this.description = description;
		this.access_right = access_right;
	}

	
	
}
