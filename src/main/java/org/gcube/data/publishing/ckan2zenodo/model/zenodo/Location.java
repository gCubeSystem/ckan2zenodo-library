package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Location {

	private Double lat;
	private Double lon;
//	@NonNull
	private String place;
	private String description;
	
}
