package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Mappings {

	
	@XmlElement
	private ResourceFilter resourceFilters;
	
	@XmlElement(name="mapping")
	private List<Mapping> mappings;
}
