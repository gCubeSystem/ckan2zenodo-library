package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@RequiredArgsConstructor
public class Creator {

//	@NonNull
	private String name;
	private String affiliation;
	private String orcid;
	private String gnd;
	public Creator(String name) {
		super();
		this.name = name;
	}
	
	
}
