package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@RequiredArgsConstructor
public class RelatedIdentifier {

	public static enum Relation{
		isCitedBy, 
		cites, 
		isSupplementTo, 
		isSupplementedBy, 
		isNewVersionOf, 
		isPreviousVersionOf, 
		isPartOf, 
		hasPart, 
		compiles, 
		isCompiledBy, 
		isIdenticalTo, 
		isAlternateIdentifier
	}
	
//	@NonNull
	private String identifier;
//	@NonNull
	private Relation relation;
	public RelatedIdentifier(String identifier, Relation relation) {
		super();
		this.identifier = identifier;
		this.relation = relation;
	}

	
	
}
