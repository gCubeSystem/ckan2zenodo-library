package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Source{
	
	@XmlElement(name="value", required= true)
	private List<Value> values=new ArrayList<>();
}