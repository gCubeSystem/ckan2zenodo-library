package org.gcube.data.publishing.ckan2zenodo.model.report;

public enum Status {

    ERROR,WARNING,PASSED
}
