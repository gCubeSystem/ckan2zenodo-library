package org.gcube.data.publishing.ckan2zenodo.model.faults;

public class ZenodoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4687842770251881142L;

	public ZenodoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZenodoException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public ZenodoException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public ZenodoException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ZenodoException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	private String remoteMessage=null;
	
	private Integer responseHTTPCode=0;
	
	public void setResponseHTTPCode(Integer responseHTTPCode) {
		this.responseHTTPCode = responseHTTPCode;
	}
	
	public Integer getResponseHTTPCode() {
		return responseHTTPCode;
	}
	
	public String getRemoteMessage() {
		return remoteMessage;
	}
	
	public void setRemoteMessage(String remoteMessage) {
		this.remoteMessage = remoteMessage;
	}
	
}
