package org.gcube.data.publishing.ckan2zenodo.model.faults;

public class TransformationException extends Exception {

	public TransformationException() {
		// TODO Auto-generated constructor stub
	}

	public TransformationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TransformationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public TransformationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TransformationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
