package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Community {

//	@NonNull
	private String identifier;

	public Community(String identifier) {
		super();
		this.identifier = identifier;
	}
	
	
}
