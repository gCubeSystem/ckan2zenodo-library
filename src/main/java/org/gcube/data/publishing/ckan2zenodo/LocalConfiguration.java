package org.gcube.data.publishing.ckan2zenodo;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocalConfiguration {



	public static class Configuration{

		public static final String THREAD_POOL_SIZE="THREAD_POOL_SIZE";
		public static final String ZENODO_ENDPOINT_CATEGORY="ZENODO_ENDPOINT_CATEGORY";
		public static final String ZENODO_ENDPOINT_PLATFORM="ZENODO_ENDPOINT_PLATFORM";
	}
	public static final Map<String,Object> defaultConfigurationMap=new HashMap<String,Object>();

	static {
		defaultConfigurationMap.put(Configuration.THREAD_POOL_SIZE, "5");
		defaultConfigurationMap.put(Configuration.ZENODO_ENDPOINT_CATEGORY,"Repository");
		defaultConfigurationMap.put(Configuration.ZENODO_ENDPOINT_PLATFORM,"Zenodo");
	}
	private static LocalConfiguration instance=null;

	@Synchronized
	public static final LocalConfiguration get() {
		if(instance==null)
			instance=new LocalConfiguration();
		return instance;
	}

	public static String getProperty(String property) {
		try{
			return (String) get().props.getOrDefault(property, defaultConfigurationMap.get(property));
		}catch(Throwable t) {
			log.warn("Unable to get configuration property "+property,t);
			return defaultConfigurationMap.get(property)+"";
		}
	}

	public Map<String,String> asMap(){
		HashMap<String,String> toReturn=new HashMap<>();		
		for(Object key :props.keySet())
			toReturn.put(key+"", (String)props.getOrDefault(key, defaultConfigurationMap.get(key)));
		return toReturn;
	}

	//***************** INSTANCE 

	Properties props;

	private LocalConfiguration() {
		props=new Properties();
		try{
			props.load(this.getClass().getResourceAsStream("/config.properties"));
		}catch(Exception e) {
			log.warn("********************** UNABLE TO LOAD PROPERTIES **********************",e);
			log.debug("Reverting to defaults : "+defaultConfigurationMap);
		}
	}


}