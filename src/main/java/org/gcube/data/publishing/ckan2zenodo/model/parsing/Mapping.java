package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Mapping {
	
	@XmlElement
	private Source source;
	
	@XmlElement
	private TargetPath targetPath;
	
	
		
	@XmlElement(name="targetElement", required= true)
	private List<TargetElement> targetElements=new ArrayList<TargetElement>();
	
	@XmlElement
	private HashMap<String,String> valueMapping=new HashMap<String, String>();
	
	
	@XmlElement
	private List<Regexp> regexp=new ArrayList<Regexp>(); 

}
