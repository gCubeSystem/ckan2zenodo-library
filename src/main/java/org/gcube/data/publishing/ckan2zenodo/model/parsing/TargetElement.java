package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetElement{	
	
	@XmlAttribute(required=false)
	private Boolean append = false;
	
	@XmlAttribute(required=false)
	private String constant= null;
	
	@XmlValue
	private String Value;
}