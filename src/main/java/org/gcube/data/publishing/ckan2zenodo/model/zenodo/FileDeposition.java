package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FileDeposition {

	private String id;
	private String filename;
	private Integer filesize;
	private String checksum;
}
