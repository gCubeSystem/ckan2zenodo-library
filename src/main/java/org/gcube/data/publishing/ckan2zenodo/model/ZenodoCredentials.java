package org.gcube.data.publishing.ckan2zenodo.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ZenodoCredentials {

	@NonNull
	private String key;
	@NonNull
	private String baseUrl;
}
