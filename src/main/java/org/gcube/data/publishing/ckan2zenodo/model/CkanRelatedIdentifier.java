package org.gcube.data.publishing.ckan2zenodo.model;

import java.net.URL;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
//@RequiredArgsConstructor
@NoArgsConstructor
@ToString
public class CkanRelatedIdentifier {

	public static CkanRelatedIdentifier getZenodo(URL doi) {
		return new CkanRelatedIdentifier("URL", "isReferencedBy", doi.toString());
	}
	
//	@NonNull
	private String relatedIdentifierType;
//	@NonNull
	private String relationType;
//	@NonNull
	private String link;
	
	public Integer getZenodoId() {
		return Integer.parseInt(link.substring(link.lastIndexOf(".")+1));
	}

	public CkanRelatedIdentifier(String relatedIdentifierType, String relationType, String link) {
		super();
		this.relatedIdentifierType = relatedIdentifierType;
		this.relationType = relationType;
		this.link = link;
	}
	
	
}
