package org.gcube.data.publishing.ckan2zenodo.commons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;

public class Parsing {

	public static Configuration JSON_PATH_ALWAYS_LIST_CONFIG=null;
	public static Configuration JSON_PATH_PATHS_CONFIGURATION=null;

	static {
		JSON_PATH_ALWAYS_LIST_CONFIG= Configuration.builder().options(Option.ALWAYS_RETURN_LIST,Option.SUPPRESS_EXCEPTIONS,Option.DEFAULT_PATH_LEAF_TO_NULL).build();
		JSON_PATH_PATHS_CONFIGURATION = Configuration.builder().options(Option.AS_PATH_LIST,Option.SUPPRESS_EXCEPTIONS,Option.DEFAULT_PATH_LEAF_TO_NULL).build();
	}

	public static ObjectMapper getMapper() {
		ObjectMapper mapper=new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
//		mapper.setSerializationInclusion(Include.NON_NULL);
		return mapper;
	}

	
	public static final DocumentContext addElement(DocumentContext ctx,String path) throws ConfigurationException {
		JsonPath jPath=JsonPath.compile(path);
		if(jPath.isDefinite()) {
			String parent=path.substring(0,path.lastIndexOf("."));
			List<String> found=ctx.read(parent);
			if(found==null || found.size()==0 || found.get(0)==null) {
				//missing parent, use recursion
				addElement(ctx,parent);
			}
			// found parent, adding element
			String element=path.substring(path.lastIndexOf(".")+1);
			
			Object value=new HashMap<String,String>();
			if(element.contains("[")) {
				value=new ArrayList<Object>(Collections.singletonList(value));
				
				element=element.substring(0,element.indexOf("["));
			}

			ctx.put(parent, element, value);
			return ctx;
		}else throw new ConfigurationException("Unable to initialize non-definite path : "+path);
	}
	
}
