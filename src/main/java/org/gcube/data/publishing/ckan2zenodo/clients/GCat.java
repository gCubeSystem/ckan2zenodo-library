package org.gcube.data.publishing.ckan2zenodo.clients;

import static org.gcube.common.authorization.client.Constants.authorizationService;
import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.faults.GcatException;
import org.gcube.gcat.client.Item;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.impl.XQuery;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GCat {

	@AllArgsConstructor
	@Getter
	@Setter
	@NoArgsConstructor
	@ToString
	private static class ResolverRequest{
		private String gcube_scope;
		private String entity_context="dataset";
		private String entity_name;
	}


	public static CkanItemDescriptor getByID(String itemName) throws MalformedURLException,GcatException {
		CkanItemDescriptor toReturn=new CkanItemDescriptor(new Item().read(itemName));
		try {
			String itemUrl=toReturn.getItemUrl();
			new URL(itemUrl);
		}catch(Exception e) {
			log.debug("Invalid item url for item "+itemName+". Retrieving it.. ");
			String itemUrl=getItemUrl(toReturn);

			log.warn("Setting following item URL "+itemUrl+" on item "+toReturn.getName());
			toReturn.setItemUrl(itemUrl);
			updateItem(toReturn);
		}		
		return toReturn;
	}


	public static void updateItem(CkanItemDescriptor toUpdate) throws MalformedURLException {
		new Item().update(toUpdate.getName(), toUpdate.getContent());
	}

	public static void check() throws MalformedURLException {
		new Item().count();
	}


	private static String getItemUrl(CkanItemDescriptor toUpdate) throws GcatException {
		try{
			String baseUrl=getResolverBaseUrl();
			
			log.debug("Resolver BaseUrl : "+baseUrl);

			ResolverRequest request=new ResolverRequest(
					getCurrentScope(),"dataset",toUpdate.getName());

			log.debug("Sending request : "+request);


			Client webClient = ClientBuilder.newClient()
					.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
			webClient.register(MultiPartFeature.class);

			Response resp= webClient.target(baseUrl).
//					path("ctlg").		
					queryParam("gcube-token", SecurityTokenProvider.instance.get()).request(MediaType.TEXT_PLAIN)		
					.post(Entity.entity(request, MediaType.APPLICATION_JSON));


			if(resp.getStatus()<200||resp.getStatus()>=300) {
				String remoteMessage=resp.readEntity(String.class);
				Integer httpCode=resp.getStatus();
				GcatException e=new GcatException("Resolver Response ["+httpCode+"] : "+remoteMessage);			
				throw e;
			}else return resp.readEntity(String.class);	

		}catch(Exception e) {
			log.error("Unable to retrieve itemUrl",e);
			throw new GcatException("Unable to retrieve itemUrl",e);
		}
	}


	private static String getCurrentScope(){
		try{
			String token=SecurityTokenProvider.instance.get();
			log.debug("Token is : "+token);
			if(token==null) throw new Exception("Security Token is null");
			AuthorizationEntry entry = authorizationService().get(token);
			return entry.getContext();
		}catch(Exception e ){
			log.debug("Unable to resolve token, checking scope provider..",e);
			return ScopeProvider.instance.get();
		}
	}


	private static String getResolverBaseUrl() throws Exception {
		try {
			XQuery query = queryFor(ServiceEndpoint.class);

			query.addCondition("$resource/Profile/Name/text() eq 'HTTP-URI-Resolver'").setResult("$resource/Profile/AccessPoint");

			DiscoveryClient<AccessPoint> client = clientFor(AccessPoint.class);

			List<AccessPoint> endpoints = client.submit(query);

			if (endpoints.size() == 0)
				throw new Exception("No Resolver available");

			//Building map of Access Points linked for EntryName
			Map<String, AccessPoint> mapEntryNameForAP = new HashMap<String, AccessPoint>(endpoints.size());

			//Printing all access points
			for (AccessPoint accessPoint : endpoints) {
				mapEntryNameForAP.put(accessPoint.name(), accessPoint);

				//Adding couple (EntryName,AccessPoint)
				Map<String, Property> propertyMap = accessPoint.propertyMap();

				if(propertyMap!=null) {

					for (String key: propertyMap.keySet()) {
						System.out.println("Property value: "+propertyMap.get(key).value());
						//...

					}
				}
			}

			//Direct access to endpoint for Catalogue Resolver 
			AccessPoint accessPoint = mapEntryNameForAP.get("ctlg");
			return accessPoint.address();
		}catch(Throwable t) {
			throw new Exception("Unable to retrieve resolver URL ",t);
		}
	}
}
