package org.gcube.data.publishing.ckan2zenodo.model;

public enum UploadPolicy {

	SKIP_EXISTING,DELETE_EXISTING,DELETE_ALL
	
}
