package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetPath {

	
	public static enum ElementType {
		map,array
	}
	
	
	
	@XmlAttribute(required=false)
	private ElementType type=ElementType.map;
	
	@XmlAttribute(required=false)
	private Boolean append=true;
	
	@XmlValue
	private String value;
}
