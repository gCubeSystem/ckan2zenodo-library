package org.gcube.data.publishing.ckan2zenodo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.List;

import org.gcube.data.publishing.ckan2zenodo.commons.Parsing;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.Commons;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Fixer {

	
	
	private static DateTimeFormatter INCOMING_FORMATTER=null;
	private static DateTimeFormatter INTERNAL_FORMATTER=null;




	
	
	static {
		INCOMING_FORMATTER=DateTimeFormatter.ofPattern("[yyyy-MM-dd['T'HH:mm:ss[.SSSSSS[z][Z][XXX]]]]");
		INTERNAL_FORMATTER=DateTimeFormatter.ofPattern(Commons.ISO_DATE_PATTERN);
		
	}



	public static final String fixIncoming(String toFix) {
		DocumentContext ctx=JsonPath.using(Parsing.JSON_PATH_ALWAYS_LIST_CONFIG).parse(toFix);
		DocumentContext pathCtx=JsonPath.using(Parsing.JSON_PATH_PATHS_CONFIGURATION).parse(toFix);

		ctx=fixIncomingDate(ctx,pathCtx, "$.created");
		ctx=fixIncomingDate(ctx,pathCtx, "$.modified");
		ctx=fixIncomingDate(ctx,pathCtx, "$..publication_date");
		ctx=fixIncomingDate(ctx,pathCtx, "$..embargo_date");

		return ctx.jsonString();
	}


	/**
	 * It seems never used to me - by Francesco M.
	 * @param toFix
	 * @return
	 */
	public static String fixSending(String toFix) {
		DocumentContext ctx=JsonPath.using(Parsing.JSON_PATH_ALWAYS_LIST_CONFIG).parse(toFix);
		DocumentContext pathCtx=JsonPath.using(Parsing.JSON_PATH_PATHS_CONFIGURATION).parse(toFix);

		ctx=fixOutgoingDate(ctx,pathCtx, "$.created");
		ctx=fixOutgoingDate(ctx,pathCtx, "$.modified");
		ctx=fixOutgoingDate(ctx,pathCtx, "$..publication_date");
		ctx=fixOutgoingDate(ctx,pathCtx, "$..embargo_date");


		return ctx.jsonString();
	}

	//*** INCOMINGs
	private static final DocumentContext fixIncomingDate(DocumentContext valuesCtx,DocumentContext pathCtx,String toFixPath) {
		try {			
			List<String> values=valuesCtx.read(toFixPath);
			List<String> paths=pathCtx.read(toFixPath);
			if(values!=null)
				for(int i=0;i<values.size();i++) {
					String toFix=values.get(i);
					if(toFix!=null) {
						JsonPath path=JsonPath.compile(paths.get(i));
						valuesCtx.set(path, fixIncomingDateString(toFix));
					}
				}

		}catch(Throwable e) {
			log.warn("Unable to fix "+toFixPath+". Cause : ",e);
		}
		return valuesCtx;
	}

	/**
	 * Updated by Francesco M., see #26166
	 * @param toFix
	 * @return
	 * @throws Exception
	 */
	public static final String fixIncomingDateString(String toFix) throws Exception {
		
		TemporalAccessor accessor=INCOMING_FORMATTER.parse(toFix);
		ZonedDateTime zTime=null;
		if(accessor.isSupported(ChronoField.HOUR_OF_DAY))
			zTime = LocalDateTime.from(accessor).atZone(ZoneOffset.UTC);			
		else {
			LocalDate date=LocalDate.from(accessor);
			//original code
			//zTime=LocalDateTime.of(date, LocalTime.of(0, 0, 0, 0)).atZone(ZoneOffset.UTC);
			
			//new code by Francesco M. returning a date without the Time
			return date.toString();
		}
		return zTime.format(INTERNAL_FORMATTER);

	}


	//*** OUTGOINGs

	private static final DocumentContext fixOutgoingDate(DocumentContext valuesCtx,DocumentContext pathCtx,String toFixPath) {
		try{
			List<String> values=valuesCtx.read(toFixPath);
			List<String> paths=pathCtx.read(toFixPath);
			if(values!=null)
				for(int i=0;i<values.size();i++) {
					String toFix=values.get(i);
					if(toFix!=null) {
						JsonPath path=JsonPath.compile(paths.get(i));
						valuesCtx.set(path, toFix.substring(0,toFix.length()-2)+":"+
								toFix.substring(toFix.length()-2, toFix.length()));
					}
				}			

		}catch(Throwable t) {
			log.warn("Unable to fix "+toFixPath+".Cause : ",t);			
		}
		return valuesCtx;
	}
}
