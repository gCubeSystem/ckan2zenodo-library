package org.gcube.data.publishing.ckan2zenodo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CkanResource{

	private String mimetype;
	private String cache_url;
	private String hash;
	private String description;
	private String name;
	private String format;
	private String url;
	private String datastore_active;
	private String cache_last_updated;
	private String package_id;
	private String created;
	private String state;
	private String mimetype_inner;
	private String last_modified;
	private String position;
	private String revision_id;
	private String url_type;
	private String id;
	private String resource_type;
	private String size;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CkanResource other = (CkanResource) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
	
	
}
