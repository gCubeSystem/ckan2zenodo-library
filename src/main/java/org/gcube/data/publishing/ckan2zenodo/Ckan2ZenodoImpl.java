package org.gcube.data.publishing.ckan2zenodo;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.data.publishing.ckan2zenodo.clients.FileUploaderManager;
import org.gcube.data.publishing.ckan2zenodo.clients.GCat;
import org.gcube.data.publishing.ckan2zenodo.clients.UploadFilesCall;
import org.gcube.data.publishing.ckan2zenodo.clients.Zenodo;
import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.CkanRelatedIdentifier;
import org.gcube.data.publishing.ckan2zenodo.model.CkanResource;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.GcatException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.InvalidItemException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.TransformationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ZenodoException;
import org.gcube.data.publishing.ckan2zenodo.model.report.EnvironmentReport;
import org.gcube.data.publishing.ckan2zenodo.model.report.Status;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Ckan2ZenodoImpl implements Ckan2Zenodo{


	private Zenodo z=null;

	private synchronized Zenodo getZenodo() throws ConfigurationException {
		if(z==null)
			z=Zenodo.get();
		return z;
	}

	@Override
	public CkanItemDescriptor read(String itemName) throws GcatException {
		try{
			return GCat.getByID(itemName);
		}catch(GcatException e) {
			throw e;
		}catch(Throwable e) {
			log.warn("Unable to load "+itemName+" from gCat",e);
			throw new GcatException("Unable to load item from gCat");
		}
	}

	@Override
	public ZenodoDeposition translate(CkanItemDescriptor desc) throws InvalidItemException, ZenodoException, ConfigurationException, TransformationException {
		return translate(desc,false);
	}

	@Override
	public ZenodoDeposition updatedMetadata(ZenodoDeposition toUpdate) throws ZenodoException, ConfigurationException {
		Zenodo z=getZenodo();
		//initialize if not created
		if(toUpdate.getSubmitted()==null) {
			ZenodoDeposition created=z.createNew();
			created.setMetadata(toUpdate.getMetadata());
			toUpdate=created;
		}
		if(toUpdate.getSubmitted()&&toUpdate.getState().equals("done"))
			z.unlockPublished(toUpdate.getId());

		ZenodoDeposition updated=z.updateMetadata(toUpdate);
		if(toUpdate.getSubmitted())
			return z.publish(updated);
		else return updated;

	}

	@Override
	public List<CkanResource> filterResources(CkanItemDescriptor desc) throws TransformationException {
		Translator tr=null;
		try{
			tr=new TransformerManager().getByProfile(desc.getProfile());
		}catch(ConfigurationException e) {
			log.warn("Forcing default resource filtering. ",e);
			tr=new Translator();
		}
		return tr.filterResources(desc);
	}

	@Override
	public Future<ZenodoDeposition> uploadFiles(Set<CkanResource> toUpload, ZenodoDeposition deposition) throws ZenodoException, ConfigurationException {
		final Zenodo z=getZenodo();

		UploadFilesCall call=new UploadFilesCall(toUpload,deposition,z);

		return FileUploaderManager.submitForDeposition(call);

	}



	@Override
	public ZenodoDeposition publish(ZenodoDeposition dep, CkanItemDescriptor toUpdate) throws ZenodoException, ConfigurationException, InvalidItemException, MalformedURLException {
		Zenodo z=getZenodo();
		if(dep.getSubmitted()&&dep.getState().equals("done"))
			z.unlockPublished(dep.getId());
		// Publishing to zenodo
		ZenodoDeposition toReturn =z.publish(dep);
		// Updateing item

		toUpdate.setZenodoDoi(CkanRelatedIdentifier.getZenodo(toReturn.getDOIUrl()));
		GCat.updateItem(toUpdate);

		return toReturn;
	}

	@Override
	public EnvironmentReport checkEnvironment() {
		log.info("Checking environment.. ");
		long start=System.currentTimeMillis();
		EnvironmentReport report=new EnvironmentReport();
		report.setContext(ScopeProvider.instance.get());
		// check gCat presence
		String gCatCheck="GCat Presence : ";
		try{
			GCat.check();
			gCatCheck+= "OK";
			report.getReportItems().put(gCatCheck, Status.PASSED);
		}catch(Throwable t){
			gCatCheck+= "Unable to contact gCat : "+t.getMessage();
			report.getReportItems().put(gCatCheck, Status.ERROR);
		}
		log.info("Checked GCat presence in "+(System.currentTimeMillis()-start)+"ms");


		// check zenodo credentials
		String zenodoCheck="Zenodo : ";
		try{
			Zenodo.get();
			zenodoCheck+= "OK";
			report.getReportItems().put(zenodoCheck, Status.PASSED);
		}catch(Throwable t){
			zenodoCheck+= "Unable to contact Zenodo : "+t.getMessage();
			report.getReportItems().put(zenodoCheck, Status.ERROR);
		}
		log.info("Environment report produced in "+(System.currentTimeMillis()-start)+"ms");
		return report;
	}

	@Override
	public ZenodoDeposition forceTranslation(CkanItemDescriptor desc) throws InvalidItemException, ZenodoException, TransformationException {
		try {
			return translate(desc,true);
		} catch (ConfigurationException e) {
			log.error("Unexpected Exception. ",e);
			throw new InvalidItemException("Unexpected exception. Please try again later");
		}
	}



	// LOGIC 

	private ZenodoDeposition translate(CkanItemDescriptor desc, boolean skipErrors) throws InvalidItemException, ZenodoException, ConfigurationException, TransformationException {
		ZenodoDeposition toUpdate=null;
		Translator tr=null;
		try {
			CkanRelatedIdentifier doi=desc.getZenodoDoi();
			if(doi!=null) {
				Zenodo z=getZenodo();
				toUpdate=z.readDeposition(doi.getZenodoId());			
			}		
			tr=new TransformerManager().getByProfile(desc.getProfile());			
		}catch (InvalidItemException e) {
			if(!skipErrors) throw e;
		} catch (ZenodoException e) {
			if(!skipErrors) throw e;
		} catch (ConfigurationException e) {
			if(!skipErrors) throw e;
			else tr=new Translator();
		}
		
		/* original
		 * return tr.transform(desc, toUpdate);
		 */
		//Added logs by Francesco M.
		ZenodoDeposition deposition = tr.transform(desc, toUpdate);
		log.debug("returning from translate: {}",deposition);
		return deposition;
		
		
	}

}
