package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class ZenodoDeposition {

	@JsonFormat(pattern = Commons.ISO_DATE_PATTERN)
	private Date created; 
	private String doi;
	private String doi_url;
	private DepositionLinks links;
	private ArrayList<FileDeposition> files;
	private Integer id;
	private DepositionMetadata metadata;
	@JsonFormat(pattern = Commons.ISO_DATE_PATTERN)
	private Date modified;
	private Integer owner;
	private Integer record_id;
	private URL record_url;
	private String state;
	private Boolean submitted;
	private String title;
	
	public URL getDOIUrl() throws MalformedURLException {
		try {			
			return new URL(doi_url);
		}catch(Throwable t) {
			log.warn("Returned concept link is broken, forming it from doi..");
			return new URL("https://doi.org/"+doi);
		}
	}
}
