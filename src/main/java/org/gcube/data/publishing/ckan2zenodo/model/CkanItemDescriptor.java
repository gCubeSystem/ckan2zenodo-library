package org.gcube.data.publishing.ckan2zenodo.model;

import java.io.IOException;
import java.util.List;

import org.gcube.data.publishing.ckan2zenodo.commons.Parsing;
import org.gcube.data.publishing.ckan2zenodo.model.faults.InvalidItemException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.minidev.json.JSONObject;

@RequiredArgsConstructor
@NoArgsConstructor
@ToString
public class CkanItemDescriptor {

	static private ObjectMapper mapper;
	
	static {
		mapper=Parsing.getMapper();
	}
	
	
	private static final String PROFILE="$.extras[?(@.key=='system:type')].value";
	private static final String TITLE="$.title";
	private static final String NOTES="$.notes";
	private static final String IS_OPEN="$.isopen";
	private static final String LICENSE_ID="$.license_id";
	private static final String LICENSE_TITLE="$.license_title";
	private static final String LICENSE_URL="$.license_url";
	private static final String TAGS="$.tags..display_name";
	private static final String VRE="$.organization.title";
	private static final String VERSION="$.version";
	private static final String METADATA_CREATED="$.metadata_created";
	private static final String METADATA_MODIFIED="$.metadata_modified";
	private static final String AUTHOR="$.author";
	private static final String MAINTAINER="$.maintainer";
	private static final String ITEM_URL="$.extras[?(@.key=='Item URL')].value";
	private static final String NAME="$.name";
	private static final String ZENODO_DOI="$.extras[?(@.key=='relatedIdentifier:Zenodo.DOI')]";
	private static final String ZENODO_DOI_VALUE="$.extras[?(@.key=='relatedIdentifier:Zenodo.DOI')].value";
	private static final String ORGANIZATION="$.organization";
	private static final String RESOURCES="$.resources";
	private static final String RESOURCES_COUNT="$.num_resources";
	private static final String ID="$.id";
	
	
	@NonNull
	@Getter
	private String content;
	
	
	@Getter(lazy=true)
	private final DocumentContext document=document();
	
	
	private DocumentContext document() {
		return JsonPath.parse(content);		
	}
	
	
	public String getProfile() {
		List<String> values=getDocument().read(PROFILE);
		if(values==null) return null;
		return values.get(0);
	}
	
	public String getName() {
		return getDocument().read(NAME);
	}
	
	public String getTitle() {
		return getDocument().read(TITLE);
	}
	
	public String getNotes() {
		return getDocument().read(NOTES);
	}
	
	public Boolean isOpen() {
		return getDocument().read(IS_OPEN);		
	}
	
	public String getLicenseId() {
		return getDocument().read(LICENSE_ID);		
	}
	
	public String getLicenseTitle() {
		return getDocument().read(LICENSE_TITLE);
	}
	
	public String getLicenseURL() {
		return getDocument().read(LICENSE_URL);
	}
	
	public List<String> getTags() {
		return getDocument().read(TAGS);
	}
	
	public String getVRE() {
		return getDocument().read(VRE);
	}
	
	public String getVersion() {
		return getDocument().read(VERSION);
	}
	
	public String getMetadataCreated() {
		return getDocument().read(METADATA_CREATED);
	}
	
	public String getMetadataModified() {
		return getDocument().read(METADATA_MODIFIED);
	}
	
	public String getAuthor() {
		return getDocument().read(AUTHOR);
	}
	
	public String getMaintainer() {
		return getDocument().read(MAINTAINER);
	}
	
	public String getItemUrl() {
		List<String> values=getDocument().read(ITEM_URL);
		if(values==null) return null;
		return values.get(0);
	}
	
	private String readRelatedIdentifierString() throws InvalidItemException {
		List<String> values=getDocument().read(ZENODO_DOI_VALUE);
		if(values==null || values.isEmpty()) return null;
		if(values.size()>1) throw new InvalidItemException("Multiple Zenodo Doi found on item.");
		return values.get(0);
	}
	
	public CkanRelatedIdentifier getZenodoDoi() throws InvalidItemException {
		try{
			String s=readRelatedIdentifierString();
			if(s==null) return null;
			return mapper.readValue(s, CkanRelatedIdentifier.class);
		}catch(InvalidItemException e) {
			throw e;
		}catch(Throwable t) {
			throw new InvalidItemException("Unable to parse item ",t);
		}
	}
	
	public CkanItemDescriptor setZenodoDoi(CkanRelatedIdentifier toSet) throws InvalidItemException {		
		try{
			String serialized=mapper.writeValueAsString(toSet);
			
			if(readRelatedIdentifierString()!=null) {
				getDocument().put(ZENODO_DOI, "value", serialized);
			}else {
			
			JSONObject obj=new JSONObject();
			obj.put("key", "relatedIdentifier:Zenodo.DOI");
			obj.put("value", serialized);		
			
			getDocument().add("$.extras",obj);
			content=getDocument().jsonString();}
			return this;
		}catch(Throwable t) {
			throw new InvalidItemException("Unable to set DOI",t);
		}
	}
	
	
	public CkanResource getResources() throws JsonProcessingException, IOException{
		return getDocument().read(RESOURCES,CkanResource.class);
//		ArrayList<CkanResource> toReturn=new ArrayList<>();
//		
//		for(Object o: mapper.readerFor(CkanResource.class).readValues().readAll())
//			toReturn.add((CkanResource) o);
//		return toReturn;
	}
	
	public CkanItemDescriptor cleanResources() {
		getDocument().set(RESOURCES,new Object[0]);
		getDocument().set(RESOURCES_COUNT,0);
		content=getDocument().jsonString();
		return this;
	}
	
	
	public CkanItemDescriptor removeOrganization() {
		getDocument().set(ORGANIZATION,null);
		content=getDocument().jsonString();
		return this;
	}
	
	public CkanItemDescriptor removeId() {
		getDocument().set(ID,null);
		content=getDocument().jsonString();
		return this;
	}
	
	public CkanItemDescriptor setItemUrl(String toSet) {
		getDocument().set(ITEM_URL,toSet);
		content=getDocument().jsonString();
		return this;
	}
	
	
}
