package org.gcube.data.publishing.ckan2zenodo;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import org.gcube.data.publishing.ckan2zenodo.model.CkanItemDescriptor;
import org.gcube.data.publishing.ckan2zenodo.model.CkanResource;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.GcatException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.InvalidItemException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.TransformationException;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ZenodoException;
import org.gcube.data.publishing.ckan2zenodo.model.report.EnvironmentReport;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;

public interface Ckan2Zenodo {

	
	/**
	 * Loads a CkanItem identified by @param itemName from current VRE gCat
	 * 
	 * @param itemName
	 * @return
	 * @throws GcatException 
	 */
	public CkanItemDescriptor read(String itemName) throws GcatException;
	
	/**
	 * Translates @param desc into a ZenodoDeposition using mappings declared in current VRE and previous existing Zenodo Deposition if any
	 * 
	 * @param desc
	 * @return
	 * @throws InvalidItemException 
	 * @throws ZenodoException 
	 * @throws ConfigurationException 
	 * @throws TransformationException 
	 */
	public ZenodoDeposition translate(CkanItemDescriptor desc) throws InvalidItemException, ZenodoException, ConfigurationException, TransformationException;
	
	/**
	 * Forces translation of passed @param desc without raising Configuration Exception
	 * 
	 * @param desc
	 * @return	 * 
	 * @throws TransformationException 
	 * @throws ZenodoException 
	 * @throws InvalidItemException 
	 */
	public ZenodoDeposition forceTranslation(CkanItemDescriptor desc) throws InvalidItemException, ZenodoException, TransformationException;
	
	
	/**
	 * Create / update metadata of @param toUpdate into the Zenodo instance declared in current VRE
	 * 
	 * @param toUpdate
	 * @return
	 * @throws ZenodoException 
	 * @throws ConfigurationException 
	 */
	public ZenodoDeposition updatedMetadata(ZenodoDeposition toUpdate) throws ZenodoException, ConfigurationException;
	
	/**
	 * Filters ckan resources of @param desc by using mappings declared in current VRE 
	 * 
	 * @param desc
	 * @return
	 * @throws ConfigurationException 
	 * @throws TransformationException 
	 */
	public List<CkanResource> filterResources(CkanItemDescriptor desc) throws TransformationException;
	
	/**
	 * Same as uploadFiles(Set<CkanResource> toUpload,ZenodoDeposition deposition,UploadPolicy policy) with default policy
	 * 
	 * @param toUpload
	 * @param deposition
	 * @return
	 * @throws ZenodoException 
	 * @throws ConfigurationException 
	 */
	public Future<ZenodoDeposition> uploadFiles(Set<CkanResource> toUpload,ZenodoDeposition deposition) throws ZenodoException, ConfigurationException;
	
	
	/**
	 * Publishes @param dep, setting/updateing DOI reference into @param toUpdate
	 * 
	 * @param dep
	 * @param toUpdate
	 * @return
	 * @throws ZenodoException 
	 * @throws ConfigurationException 
	 * @throws InvalidItemException 
	 * @throws MalformedURLException 
	 */
	public ZenodoDeposition publish(ZenodoDeposition dep, CkanItemDescriptor toUpdate) throws ZenodoException, ConfigurationException, InvalidItemException, MalformedURLException;
		
	
	/**
	 * Checks environment configuration
	 * 
	 *  -gCat is present
	 *  -Zenodo credentials are present
	 *  
	 * 
	 * 
	 */

	public EnvironmentReport checkEnvironment();
}
