package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DepositionLinks {

	private String badge;
	private String bucket;
	private String conceptbadge;
	private String conceptdoi;
	private String doi;
	private String html;
	private String latest;
	private String latest_html;
	private String self;
	private String latest_draft;
}
