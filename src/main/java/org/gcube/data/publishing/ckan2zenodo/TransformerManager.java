package org.gcube.data.publishing.ckan2zenodo;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.data.publishing.ckan2zenodo.commons.IS;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.parsing.Mappings;

import lombok.Synchronized;


public class TransformerManager {



	private static DocumentBuilder builder=null;

	@Synchronized	
	private static DocumentBuilder getBuilder() throws ParserConfigurationException {
		if(builder==null) {
			DocumentBuilderFactory factory =DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
		}
		return builder;
	}

	public Translator getByProfile(String profile) throws ConfigurationException {
		for(GenericResource r: IS.queryForGenericResources("Ckan-Zenodo-Mappings")){
			if (r.profile().name().equals(profile)) {
				Mappings m = IS.readMappings(r);
				if(m == null) return new Translator();
				else return  new Translator(m);
			}
		}
		return new Translator();
	}



	
}
