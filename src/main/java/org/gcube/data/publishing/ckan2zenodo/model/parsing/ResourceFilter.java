package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceFilter {

	public static final ResourceFilter PASS_ALL=new ResourceFilter(Collections.singletonList(
			new Filter(Collections.singletonList("$.resources[?(@.format)]"))));
	
	@XmlElement(name="filter", required= false)
	private List<Filter> filters=new ArrayList<Filter>();
	
}
