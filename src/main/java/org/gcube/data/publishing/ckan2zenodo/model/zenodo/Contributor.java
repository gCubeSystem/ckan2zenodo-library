package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
//@RequiredArgsConstructor
@NoArgsConstructor
public class Contributor extends Creator {

	public static enum Type{
		ContactPerson, 
		DataCollector, 
		DataCurator, 
		DataManager,
		Distributor, 
		Editor, 
		Funder, 
		HostingInstitution, 
		Producer, 
		ProjectLeader, 
		ProjectManager, 
		ProjectMember, 
		RegistrationAgency, 
		RegistrationAuthority, 
		RelatedPerson, 
		Researcher, 
		ResearchGroup, 
		RightsHolder,
		Supervisor, 
		Sponsor, 
		WorkPackageLeader, 
		Other
	}
	
	
//	@NonNull
	private Type type;

	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contributor other = (Contributor) obj;
		if (type != other.type)
			return false;
		return true;
	}


	public Contributor(Type type) {
		super();
		this.type = type;
	}
	
	
}
