package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Value{
	public static enum ValueType{
		jsonPath,constant
	}
	
	@XmlAttribute(required=false)
	private String split;
	
	@XmlAttribute(required=false)
	private ValueType type=ValueType.constant;
	
	@XmlValue
	private String value;			
}