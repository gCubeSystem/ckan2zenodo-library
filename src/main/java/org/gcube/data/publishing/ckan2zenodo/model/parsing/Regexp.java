package org.gcube.data.publishing.ckan2zenodo.model.parsing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Regexp{
	public static enum RegexpType{
		replace,extract
	}

	private RegexpType type=RegexpType.extract;

	private String target;
	private String replacement;
}