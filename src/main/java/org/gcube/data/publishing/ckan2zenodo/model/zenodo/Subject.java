package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Subject {

//	@NonNull
	private String term;
//	@NonNull
	private String identifier;
	private String scheme;
	public Subject(String term, String identifier) {
		super();
		this.term = term;
		this.identifier = identifier;
	}
	
	
	
}
