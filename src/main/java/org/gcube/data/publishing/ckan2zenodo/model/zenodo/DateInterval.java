package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import java.util.Date;

import org.gcube.data.publishing.ckan2zenodo.commons.DateParser;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class DateInterval {
	public static enum Type{
		Collected, Valid, Withdrawn
	}
	
	//Updated by Francesco M. #26166
	@JsonFormat(pattern = Commons.ISO_DATE_PATTERN_WITHOUT_TIME)
	@JsonDeserialize(using = DateParser.class)
	private Date start;
	//Updated by Francesco M. #26166
	@JsonFormat(pattern = Commons.ISO_DATE_PATTERN_WITHOUT_TIME)
	@JsonDeserialize(using = DateParser.class)
	private Date end;
//	@NonNull
	private Type type;
	private String description;

	
}
