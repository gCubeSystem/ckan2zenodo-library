package org.gcube.data.publishing.ckan2zenodo.model.zenodo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Grant {

//	@NonNull
	private String id;
}
