package org.gcube.data.publishing.ckan2zenodo.commons;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.io.StringReader;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ConfigurationException;
import org.gcube.data.publishing.ckan2zenodo.model.parsing.Mappings;
import org.gcube.data.publishing.ckan2zenodo.model.parsing.ResourceFilter;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IS {


	public static List<GenericResource> queryForGenericResources(String secondaryType){
		log.debug("Querying for Service Endpoints [secondary type : {} ]",secondaryType);

		SimpleQuery query = queryFor(GenericResource.class);

		query.addCondition("$resource/Profile/SecondaryType/text() eq '"+secondaryType+"'");

		DiscoveryClient<GenericResource> client = clientFor(GenericResource.class);

		return client.submit(query);
	}

	public static List<ServiceEndpoint> queryForServiceEndpoints(String category, String platformName){
		log.debug("Querying for Service Endpoints [category : {} , platformName : {}]",category,platformName);

		SimpleQuery query = queryFor(ServiceEndpoint.class);

		query.addCondition("$resource/Profile/Category/text() eq '"+category+"'")
		.addCondition("$resource/Profile/Platform/Name/text() eq '"+platformName+"'");				

		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);

		return client.submit(query);
	}

	public static Mappings readMappings(GenericResource res) throws ConfigurationException{

		try {
			String body=res.profile().bodyAsString();
			
			 // create JAXB context and unmarshaller
	        Unmarshaller um=JAXBContext.newInstance(Mappings.class).createUnmarshaller();
	        

	        Mappings m= (Mappings) um.unmarshal(new StreamSource(new StringReader(body)));
			if(m.getMappings()==null) m.setMappings(Collections.emptyList());
			if(m.getResourceFilters()==null||m.getResourceFilters().getFilters().isEmpty()) m.setResourceFilters(ResourceFilter.PASS_ALL);
			return m;
		}catch(Throwable t) {
			log.debug("Error while parsing mapping from resource "+res.id()+" name : "+res.profile().name(),t);
			throw new ConfigurationException("Invaild mapping resource "+res.id()+" name : "+res.profile().name(),t);
		}
	}
	
	
//	public static ResourceFilter readResourceFilters(GenericResource res) throws ConfigurationException{
//		try{
//			ArrayList<ResourceFilter.Filter> filtersList=new ArrayList<>(); 
//			Element root=res.profile().body();
//			NodeList filters=((Element) root.getElementsByTagName("resourceFilters").item(0)).getElementsByTagName("filter");
//			for(int i=0;i<filters.getLength();i++) {
//				Element filter=(Element) filters.item(i);
//				ArrayList<String> conditions=new ArrayList<>();
//				NodeList conditionNodes=filter.getElementsByTagName("condition");
//				for(int j=0;j<conditionNodes.getLength();j++) {
//					conditions.add(conditionNodes.item(j).getTextContent());
//				}
//				if(!conditions.isEmpty())
//					filtersList.add(new ResourceFilter.Filter(conditions));
//			}
//			if(filtersList.isEmpty())
//				throw new Exception("No Filter actually declared.");
//			return new ResourceFilter(filtersList);
//		}catch(Throwable t) {
//			log.debug("Error while parsing filters from resource "+res.id()+" name : "+res.profile().name(),t);
//			throw new ConfigurationException("Invalid resource filters declared in "+res.id()+" name : "+res.profile().name(),t);
//		}
//	}
	
	public static String decryptString(String toDecrypt){
		try{
			return StringEncrypter.getEncrypter().decrypt(toDecrypt);
		}catch(Exception e) {
			throw new RuntimeException("Unable to decrypt : "+toDecrypt,e);
		}
	}
}
