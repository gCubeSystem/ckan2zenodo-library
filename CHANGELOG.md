This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
# Changelog for org.gcube.data.publishing.ckan2zenodo-library

## [v1.0.4] 2022-12-13  
- Bug fixing `publication_date=null` [#26166]


## [v1.0.3] 2023-03-28  
- Extensions evaluated from URL [#22889](https://support.d4science.org/issues/22889)



## [v1.0.2] 2021-07-30
- Introduced environemnt check [#19990](https://support.d4science.org/issues/19990) 


## [v1.0.1] 2021-04-7

- Integration with gCat 2.0.0 [#20751](https://support.d4science.org/issues/20751)

## [v1.0.0] 2021-02-18

- Improved mapping language : target record
- Improved mapping language : value iteration


## [v0.0.3-SNAPSHOT] 2020-06-30
- Changed coordinartes for gcat-client


## [v0.0.2] 2020-06-30

### Enhancements
- Default Ckan2Zenodo translation to comply with mandatory Zenodo fields [19489](https://support.d4science.org/issues/19489)
- Ckan2Zenodo library to provide means to apply default translation only [19490](https://support.d4science.org/issues/19490)
- Support to "split" on source values [19534](https://support.d4science.org/issues/19534) 
- Support to "append" to target elements [19534](https://support.d4science.org/issues/19535)
- Multiple Date Format parsing [19540](https://support.d4science.org/issues/19540)
- Automatically set item URL if missing.

### Fixes 
- Default filter resource is applied in class Ckan2ZenodoImpl.java [19528](https://support.d4science.org/issues/19528)
- Fixed error message for missing profile.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).